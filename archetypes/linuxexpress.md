---
title: "#{{ strings.TrimRight "-Linux-Express" .Name }} Linux Express"
date: {{ dateFormat "2006-01-02" .Date }}
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/{{ strings.TrimRight "-Linux-Express" .Name }}linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 
  mlength : 
  iduration : "00:MM:SS"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE{{ strings.TrimRight "-Linux-Express" .Name }}
---

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/{{ strings.TrimRight "-Linux-Express" .Name }}linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/{{ strings.TrimRight "-Linux-Express" .Name }}linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #107 [Especial 4º Aniversario](https://podcastlinux.com/PL107)
+ Siguiente episodio:
+ 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
