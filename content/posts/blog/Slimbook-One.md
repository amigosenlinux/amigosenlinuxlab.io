---
title: Slimbook One
author: Juan Febles
date: 2017-05-14
img: 2017/slimbook1.jpg
---
![]({{< ABSresource url="/images/2017/slimbook1.jpg" >}})  
Gracias a la confianza depositada por los oyentes como tú, [Slimbook](https://slimbook.es/) me ha cedido un [Slimbook One](https://slimbook.es/one-minipc-potente), su potente Mini PC para
tener el dispositivo perfecto GNU/Linux.


Pequeño, ligero, eficiente y elegante. Con él lo tienes todo: Un ultra compacto ordenador de sobremesa, un servidor de almacenamiento (NAS),
un centro multimedia y un dispositivo gaming.
![SlimBook2]({{< ABSresource url="/images/PL/2017/slimbook2.jpg" >}})

El próximo miércoles, el Linux Express irá dedicado una buena parte a esta joya y cuando pase algunas semanas realizaré
un especial. Tengo muchas ganas de ver qué prestaciones puede dar es "cacharro".
![SlimBook3]({{< ABSresource url="/images/PL/2017/slimbook3.jpg" >}})

Mis primeras impresiones; más pequeño que en las imágenes de presentación: cabe en la palma de la mano.
¿Será el dispositivo único para tener todos mis servicios en un sólo ordenador? Por ahora, contentísimo.
