---
title: "#111 Linux Express"
date: 2021-01-20
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/111linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5622588
  mlength : 5358905
  iduration : "00:11:09"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE111
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/111linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/111linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #121 [Linux Connexion con Lina Castro](https://podcastlinux.com/PL121)
+ Siguiente episodio: ESP8266
+ Hablando de Domótica en [Cacharreo Geek](https://youtu.be/iWGt2ZX_3LU)
+ Aprendiendo a soldar
+ Migración masiva de conocidos a Telegram
+ Probando [Signal](https://signal.org/es/)
+ Notas libres con [Joplin](https://joplinapp.org/)
+ Docker, asignatura pendiente
+ Nuevo [Slimbook Titán](https://slimbook.es/titan)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
