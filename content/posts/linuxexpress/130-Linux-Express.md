---
title: "#130 Linux Express"
date: 2021-10-13
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/130linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 7557008
  mlength : 7340241
  iduration : "00:15:17"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE130
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/130linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/130linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #140 [Linux Connexion con Luis del Valle](https://podcastlinux.com/PL140)
+ Siguiente episodio: [Linux Connexion con Luis del Valle 2ª parte](https://programarfacil.com/)
+ Episodio Encuentro Podcast Linux con [Jam](https://jam.systems/podcastlinux) el sábado 30 de octubre a las 19:00 h (UTC +2: España peninsular)
+ Gracias [Pedro Mosqueteroweb](https://www.ivoox.com/podcast-mosqueteroweb-tecnologia_sq_f1248962_1.html)
+ Curso sobre [Kdenlive](https://podcastlinux.com/kdenlive)
+ Anímate a participar en los [#ViernesDeEscritorio](https://twitter.com/hashtag/ViernesDeEscritorio)
+ ¿Tienes un evento o proyecto de Software Libre a promocionar? Aquí tienes [Podcast Linux](https://podcastlinux.com/)
+ Presenta tu charla en [Akademy-es](https://www.kde-espana.org/akademy-es-2021-en-linea/presenta-tu-charla) 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
