---
title: "#166 Linux Express"
date: 2023-03-01
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/166linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 7948082
  mlength : 7501054
  iduration : "00:15:21"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE166
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/166linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/166linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #176 [Hardware Raspberry Pi Pico](https://podcastlinux.com/PL176)
+ Siguiente episodio: [Linux Connexion con Raúl Caro](https://fryntiz.es)
+ [Raspberry Pi Pico Wifi](https://mastodon.social/@podcastlinux/109890711640691600) con los pines soldados
+ [Proyecto Bartop](https://mastodon.social/@podcastlinux/109901120136524877) con Raspberry Pi y Retro Pi.
+ [ALCI](https://mastodon.social/@podcastlinux/109861495352575088), Arch Linux con instalador gráfico
+ Potenciando más a [Mastodon](https://mastodon.social/@podcastlinux/109872798121549920) en mis publicaciones
+ Nuevo proyecto [Open Assistant](https://mastodon.social/@podcastlinux/109885046204746840), un chat conversacional IA con licencia libre
+ [DVD Copia de seguridad](https://mastodon.social/@podcastlinux/109851064055492139) de 2004 en casa de mis padres
+ [Kits de construcción Micro:bit](https://mastodon.social/@podcastlinux/109912409504030642) en el colegio
+ Probando [KeepassXC](https://mastodon.social/@podcastlinux/109924754908486235) como administrador de contraseñas.



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
