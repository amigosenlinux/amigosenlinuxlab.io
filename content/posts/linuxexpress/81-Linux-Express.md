---
title: "#81 Linux Express"
date: 2019-11-27
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/81linuxexpress
  olength : 5303796
  mlength : 6635520
  iduration : "00:13:33"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE81
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/81linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/81linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio #91 Linux Connexion con [Luis del Valle](https://avpodcast.net/podcastlinux/luisdelvalle)
+ Próximo episodio: Productividad en GNU/Linux
+ Probando la [Academia Programar Fácil](https://campus.programarfacil.com)
+ Cacharreando con una [matriz 4 en 1 y Arduino](https://twitter.com/podcastlinux/status/1195629876478390272)
+ Pedido varios [ESP8266](https://twitter.com/podcastlinux/status/1197229509487087618) para seguir cacharreando
+ Nuevo [Slimbook Pro X 15](https://slimbook.es/prox15)
+ Nueva versión de [Audacity 2.3.3](https://www.audacityteam.org/audacity-2-3-3-released/)
+ [Emacs](https://www.gnu.org/software/emacs/) como editor de  [Linux Express](https://podcastlinux.com/)
+ Cambios en [AvPodcast](https://avpodcast.net/)
+ Premio Accésit [Wifiteca](https://twitter.com/LaSalleLaLaguna/status/1199418269867880449) en los [II Premios Tecnoedu](https://tecnoedu.webs.ull.es/premios-2019-2/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  	
