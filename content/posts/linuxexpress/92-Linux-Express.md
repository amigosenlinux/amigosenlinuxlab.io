---
title: "#92 Linux Express"
date: 2020-04-29
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/92linuxexpress
  olength : 4521681
  mlength : 5556224
  iduration : "00:11:18"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE92
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/92linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/92linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #102 [Especial VPS Educativo Libre](https://avpodcast.net/podcastlinux/vpseducativo)
+ Siguiente episodio: Especial Directo Confinamiento
+ Realizar vídeos en directo con [Icecast](https://twitter.com/podcastlinux/status/1249364425506766850)
+ Emitir directo vídeo desde la terminal: [FFmpeg](https://twitter.com/podcastlinux/status/1248669529015758852)
+ Jugando con [LMMS](https://twitter.com/podcastlinux/status/1253650163886968833)
+ Mirando pc sobremesa AMD.
+ Llegó [Ubuntu 20.04 LTS](https://ubuntu.com/download)
+ Nuevo [KDEnLive 20.04](https://kdenlive.org/en/2020/04/kdenlive-20-04-is-out)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
