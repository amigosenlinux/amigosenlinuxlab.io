---
title: "#167 Linux Express"
date: 2023-03-15
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/167linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5075865
  mlength : 4996851
  iduration : "00:10:08"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE167
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/167linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/167linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #177 [Linux Connexion con Raúl Caro](https://podcastlinux.com/PL177)
+ Siguiente episodio: Hardware Kit Xeon Chino
+ Móntarte tu propia consola retro con [PiConsole](https://mastodon.social/@podcastlinux/109940717168883669)
+ Valorando actualizar mis podcasts al [Podcasting 2.0](https://mastodon.social/@podcastlinux/109952094398265439)
+ Me he animado a cambiar yo mismo el teclado del [Thinkpad T440P](https://mastodon.social/@podcastlinux/109964325895547218)
+ Probando [Bitwarden](https://mastodon.social/@podcastlinux/109969973531783896) como gestor de contraseñas
+ Probando [Castero](https://mastodon.social/@podcastlinux/109991728644382066), un gestor de podcast para la terminal.
+ Utiliza [Free Podcast Transcription](https://freepodcasttranscription.com), una webapp con Whisper para que de forma local obtengas la transcripción.
+ Una de las cosas que más me gustan de #ArchLinux es su [wiki](https://wiki.archlinux.org)
+ Tenemos fecha para la [Akademy-es](https://mastodon.social/@podcastlinux/109982596234913476): Málaga el 9 y 10 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
