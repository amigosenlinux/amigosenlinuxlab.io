---
title: "#152 Linux Express"
date: 2022-08-17
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/152linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5890563
  mlength : 5456290
  iduration : "00:11:21"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE152
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/152linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/152linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #162 [Linux Connexion con Juan Antonio González](https://podcastlinux.com/PL162)
+ Siguiente episodio: Linux Connexion con [Fediverse.TV](https://fediverse.tv)
+ Me encuentro con un cd de [Ubuntu 8.04](https://nitter.net/podcastlinux/status/1554360301541359618#m) 
+ Nueva versión de [Audacity](https://forum.audacityteam.org/viewtopic.php?f=68&t=126279) en septiembre
+ Montando en Audacity los audios de [Libre Home Gym](https://nitter.net/podcastlinux/status/1555093615441072130#m)
+ Nuevo temazo linuxero de [Yoyo](https://nitter.net/yoyo308/status/1555783952123707392#m) 
+ Terminado el juego [Stevedore](https://nitter.net/podcastlinux/status/1556905197145817088#m)
+ ¿Un curso sobre [LMMS](https://lmms.io/)?
+ [Jpod](https://jpod.es) en Madrid 
+ [Akademy-es](https://www.kdeblog.com/presentado-akademy-es-2022-en-barcelona-y-en-linea-akademyes.html) y [Akademy](https://akademy.kde.org/2022) en Barcelona en octubre 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
