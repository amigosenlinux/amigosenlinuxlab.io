---
title: "#135 Linux Express"
date: 2021-12-22
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/135linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5917304
  mlength : 5489100
  iduration : "00:11:25"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE135
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/135linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/135linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #145 [Hardware Powkiddy Q90](https://podcastlinux.com/PL144)
+ Siguiente [Hardware Vant Agile](https://www.vantpc.es/agile)
+ Cacharreando con consolas de 2º mano para instalarle RetroArch
+ Evento [24H24L](https://24h24l.org/)
+ Termina el curso [Kdenlive](https://podcastlinux.com/kdenlive)
+ [Proyecto sonoro Libre Home Gym](https://twitter.com/podcastlinux/status/1464519831387000836) para realizar ejercicios en casa
+ Probando nuevas [voces libres](https://twitter.com/alfonsoem/status/1468866963019644928)
+ Cambio de intro y música para el 2022 


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
