---
title: "#142 Linux Express"
date: 2022-03-30
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/142linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6319343
  mlength : 5924404
  iduration : "00:12:20"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE142
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/142linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/142linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #152 [Paqueterías Multidistribución GNU/Linux](https://podcastlinux.com/PL152)
+ Siguiente episodio: Linux Connexion con Atareao
+ Encuesta del [sistema de paquetería GNU/Linux que gusta más](https://twitter.com/podcastlinux/status/1506133049070735365)
+ Volcar vídeos de Youtube en Fediverse.tv
+ LMDE 5 para [equipos viejunos](https://twitter.com/raivenra/status/1506288227505905666)
+ Reciclando componentes de audio 
+ Minicurso de [Automatización del Hogar](https://programarfacil.com/domotica-para-gente-y-casas-inteligentes/)
+ [Flisol Tenerife 2022](https://flisol.info/FLISOL2022/Espana/Tenerife) el 23 de abril
+ [esLibre](https://eslib.re/2022) presencial en Vigo el 24 y 25 de junio

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
