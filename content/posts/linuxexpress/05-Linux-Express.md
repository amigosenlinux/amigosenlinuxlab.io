---
title: "#05 Linux Express"
date: 2017-01-25
author: Juan Febles
categories: [linuxexpress]
img: 2017/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/05linuxexpress
  olength : 2613623
  mlength : 2411697
  iduration : "00:05:01"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE05
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/05linuxexpress.mp3" type="audio/mpeg">
</audio>

El 5º linux Express, los audios que comparto cada 2 semanas en [Telegram](https://t.me/podcastlinux) para ir alternando
los podcasts quincenales con éstos.
Aquí encontrarás información de lo que estoy proyectando y realizando mientras esperas a un nuevo podcast.
