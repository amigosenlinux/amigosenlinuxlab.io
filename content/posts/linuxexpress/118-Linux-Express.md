---
title: "#118 Linux Express"
date: 2021-04-28
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/118linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6016735
  mlength : 5860665
  iduration : "00:12:12"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE118
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/118linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/118linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #128 [Especial FLISoL Tenerife 2021](https://podcastlinux.com/PL128)
+ Siguiente episodio: Hardware ThinkPad
+ Análisis evento [FLISOL Tenerife 2021](https://flisol.info/FLISOL2021/Espana/Tenerife)
+ Estar o no en redes sociales privativas, el eterno debate.
+ Servicio [CryptDrive](https://docs.cryptpad.fr/en/user_guide/drive.html)
+ Anotaciones de pantallazos con [Spectacle](https://www.linuxadictos.com/asi-es-el-editor-de-anotaciones-de-spectacle-20-12-perfecto-para-los-usuarios-de-kde.html)
+ [WSL](https://www.muylinux.com/2021/04/22/wslg-aplicaciones-linux-en-windows/) ejecuta aplicaciones de Linux en Windows
+ Podcast [Somos Tecnológicos](https://www.ivoox.com/podcast-somos-tecnologicos_sq_f11062739_1.html)
+ Eventos a tener en cuenta: [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
