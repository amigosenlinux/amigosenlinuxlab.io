---
title: "#128 Linux Express"
date: 2021-09-15
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/128linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5366312
  mlength : 5156822
  iduration : "00:10:44"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE128
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/128linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/128linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #138 [Hardware Slimbook AMD](https://podcastlinux.com/PL138)
+ Siguiente episodio: [Domótica Libre con Home Assistant](https://www.home-assistant.io/)
+ Curso sobre [Kdenlive](https://podcastlinux.com/kdenlive)
+ Utilizando [FET](https://lalescu.ro/liviu/fet/) para hacer los horarios del cole.
+ Permisos [root en Dolphin](https://twitter.com/podcastlinux/status/1434129707990147075)
+ Corregido bug en [Kasts](https://apps.kde.org/es/kasts/)
+ charla de #EntreTuxes sobre [Feeds con David Marzal y Audio Hi-Res con Jorge Lama](https://devtube.dev-wiki.de/videos/watch/bdf3732a-e68d-4b3c-a9e6-68c91d970e80)
+ Presenta tu charla en [Akademy-es](https://www.kdeblog.com/presenta-tu-charla-a-akademy-es-2021.html) 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
