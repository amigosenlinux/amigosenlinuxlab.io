---
title: "#172 Linux Express"
date: 2023-05-24
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/172linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6489094
  mlength : 5919382
  iduration : "00:12:11"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE172
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/172linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/172linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #181 [6 meses con Arch Linux](https://podcastlinux.com/PL182)
+ Siguiente episodio: [Linux Connexion con Yoyo Fernández](salmorejogeek.com)
+ Qué rabia utilizar [las ventanas](https://mastodon.social/@podcastlinux/110336842234139340)
+ Utilizando [Xiaomi Tool V2](https://mastodon.social/@podcastlinux/110348166799954931) para desbloquear Móviles Xiaomi
+ [Liberando un Android](https://mastodon.social/@podcastlinux/110400071352961088) desde la terminal
+ Flasheando Custom Roms desde [Xiaomi Tool V2](https://mastodon.social/@podcastlinux/110405733790840212)
+ Toda la información de las paqueterías [pacman y AUR](https://mastodon.social/@podcastlinux/110360435602093917)
+ [Cambios en los repositorios de Arch Linux](https://mastodon.social/@podcastlinux/110387803232122983) este fin de semana
+ Probando [Vega Chess](https://mastodon.social/@podcastlinux/110366097593926337) para realizar emparejamientos de torneos en GNU/Linux
+ Probando [Rclone](https://mastodon.social/@podcastlinux/110376478369129439) para tener copias de seguridad en la nube
+ Evento [Akademy-es](https://mastodon.social/@podcastlinux/109982596234913476): Málaga el 9 y 10 de junio

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
