---
title: "#131 Linux Express"
date: 2021-10-27
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/131linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5830952
  mlength : 5698915
  iduration : "00:11:52"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE131
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/131linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/131linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #141 [Linux Connexion con Luis del Valle 2](https://podcastlinux.com/PL141)
+ Siguiente episodio Encuentro Podcast Linux con [Jam](https://jam.systems/podcastlinux) el sábado 30 de octubre a las 19:00 h (UTC +2: España peninsular)
+ Temática de próximos episodios
+ Curso sobre [Kdenlive](https://podcastlinux.com/kdenlive)
+ Presenta tu charla en [Akademy-es](https://www.kde-espana.org/akademy-es-2021-en-linea/presenta-tu-charla) 
+ [KDE Plasma 5.23 y 25º aniversario](https://www.kdeblog.com/5-novedades-de-plasma-5-23-edicion-25-aniversario.html)
+ [Ubuntu 21.10](https://blog.desdelinux.net/ubuntu-21-10-impish-indri-llega-con-actualizaciones-nuevo-instalador-y-mas/)
+ Gracias [Yoyo](https://salmorejogeek.com/)
+ Podcast [Despeja la X](https://www.xataka.com/tag/despeja-la-x) y la [Guerra de los Chips](https://www.xataka.com/componentes/guerra-chips-como-apple-google-estan-dandole-cana-a-intel-qualcomm-toda-vida-podcast-despeja-x-159)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
