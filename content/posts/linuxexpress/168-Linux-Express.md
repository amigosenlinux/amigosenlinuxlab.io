---
title: "#168 Linux Express"
date: 2023-03-29
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/168linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5435913
  mlength : 5193710
  iduration : "00:10:33"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE168
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/168linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/168linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #178 [Hardware Kit Xeon Chino](https://podcastlinux.com/PL178)
+ Siguiente episodio: Linux Connexion con Pedro Mosquetero Web
+ Componentes del [2º Kit Xeon Chino](https://mastodon.social/@podcastlinux/110019988983744475) que monto
+ Me he animado a comprar un [SSD NVMe de 1TB](https://mastodon.social/@podcastlinux/110031313340519351)
+ [Buscando distros madres](https://mastodon.social/@podcastlinux/110043582009901003) para un 2º episodio de Distros no Debianitas
+ Desconocía que [archinstall](https://mastodon.social/@podcastlinux/110049244029320606) admitía archivo de configuración
+ [Spectacle](https://mastodon.social/@podcastlinux/110059624979897478) es un espectáculo
+ Probando [MastoMetrics](https://mastodon.social/@podcastlinux/110071059768627377) para analizar mejor mis publicaciones  en Mastodon.
+ Ya tengo mis podcasts conectados a [Op3](https://mastodon.social/@podcastlinux/110088644518524517)
+ [Linux Command Library](https://mastodon.social/@podcastlinux/110083218038927687) para hacerte con la terminal
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 12 y 13  de mayo
+ Tenemos fecha para la [Akademy-es](https://mastodon.social/@podcastlinux/109982596234913476): Málaga el 9 y 10 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
