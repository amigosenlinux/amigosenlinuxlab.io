---
title: "#162 Linux Express"
date: 2023-01-04
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/162linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6734206
  mlength : 6379670
  iduration : "00:13:01"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE162
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/162linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/162linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #172 [Linux Connexion con Rafa Laguna](https://podcastlinux.com/PL172)
+ Siguiente episodio:[Especial PC Reciclado de la Audiencia](https://podcastlinux.com/pcreciclado)
+ Mantenimiento de componentes para el [#PCRecicladoDeLaAudiencia](https://nitter.net/podcastlinux/status/1605797782207401986#m)
+ Anímate a contribuir en [PodLi, the Podcast Linux Game](https://gitlab.com/podcastlinux/pocastlinuxgame)
+ Todos mis ordenadores con [Arch Linux](https://nitter.net/podcastlinux/status/1608696885769822209#m)
+ Encuestas sobre uso de [Debian o derivadas](https://nitter.net/podcastlinux/status/1606948062810750976#m)
+ [Octopi](https://nitter.net/podcastlinux/status/1607609848127954946#m) para actualizar e instalar en Arch Linux de forma sencilla
+ Nueva versión de [Audacity 3.2.3](https://github.com/audacity/audacity/releases/tag/Audacity-3.2.3)
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 5 y 6 de mayo


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
