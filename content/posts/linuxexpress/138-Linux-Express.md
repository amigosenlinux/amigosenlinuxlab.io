---
title: "#138 Linux Express"
date: 2022-02-02
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/138linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5214458
  mlength : 4921093
  iduration : "00:10:14"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE138
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/138linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/138linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #148 [Linux Connexion con Pablo López](https://podcastlinux.com/PL148)
+ Siguiente episodio: [Slimbook Executive](https://slimbook.es/executive)
+ [Teclado RGB Slimbook](https://slimbook.es/accesorios-capturadora-4k-teclado-rgb-hub-hdmi-mini-docking#keyboard)
+ [Capturadora 4K de Slimbook](https://slimbook.es/accesorios-capturadora-4k-teclado-rgb-hub-hdmi-mini-docking)
+ [Linux Mint 20.3](https://linuxmint.com/)
+ [Proyecto Libre Home Gym](https://gitlab.com/podcastlinux/librehomegym) para realizar ejercicios en casa
+ [MOOC de Conocimiento Abierto y Software Libre](https://abierta.ugr.es/software_libre)
+ [esLibre](https://eslib.re/2022) presencial en Vigo el 24 y 25 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
