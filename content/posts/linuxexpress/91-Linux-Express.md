---
title: "#91 Linux Express"
date: 2020-04-15
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/91linuxexpress
  olength : 4503775
  mlength : 5498880
  iduration : "00:11:11"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE91
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/91linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/91linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #101 [Especial #MaratonPodEnCasa](https://avpodcast.net/podcastlinux/maratonpodencasa)
+ Siguiente episodio: Especial VPS Educativo Libre
+ Realizar directo con [Icecast](https://twitter.com/podcastlinux/status/1249364425506766850)
+ Emitir directo desde la terminal: [FFmpeg](https://twitter.com/podcastlinux/status/1248669529015758852)
+ [Directo de 2 horas](https://archive.org/details/podcastlinuxdirecto20) 100% Software Libre
+ Pc chinos con [Xenon](https://twitter.com/podcastlinux/status/1247525823541739528)
+ Potenciar [#ViernesDeEscritorio #EscritorioGNULinux](https://twitter.com/hashtag/EscritorioGNULinux)
+ Nuevo [Firefox 75](https://twitter.com/podcastlinux/status/1247587645200445445)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
