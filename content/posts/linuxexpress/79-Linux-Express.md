---
title: "#79 Linux Express"
date: 2019-10-30
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/79linuxexpress
  olength : 5422355
  mlength : 6674432
  iduration : "00:13:38"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE79
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/79linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/79linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio #89 Linux Connexion con[Irene Soria ](https://avpodcast.net/podcastlinux/ierenesoria)
+ Próximo episodio:[Arduino](https://www.arduino.cc/)
+ Mostrando  [Arduino](https://www.arduino.cc/) a mis alumnos
+ [Wifiteca](https://youtu.be/Lf4QpPS6wBY) se presenta a los [II Premios TecnoEdu](https://tecnoedu.webs.ull.es/premios-2019-2/)
+ Utilizando [ssh en git] (https://docs.gitlab.com/ee/ssh/)
+ Nueva versión [KDE Plasma 5.17](https://kde.org/announcements/plasma-5.17.0.php)
+ Próximos Eventos: [Maratón Pod](https://www.maratonpod.es/) y [OSHWDem](https://oshwdem.org/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  	
