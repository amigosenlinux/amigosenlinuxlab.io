---
title: "#175 Linux Express"
date: 2023-07-05
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/175linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5561898
  mlength : 5300385
  iduration : "00:10:53"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE175
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/175linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/175linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #184 [Especial 7ª Aniversario](https://podcastlinux.com/PL185)
+ Siguiente episodio: Linux Connexions de verano
+ [Cider](https://mastodon.social/@podcastlinux/110574659199074791) un reproductor multiplataforma de Apple Music para GNU/Linux
+ Podcast [Don Tomás](https://mastodon.social/@podcastlinux/110585983835847177) sobre el fediverso
+ Plugins [Linux Studio Plugins](https://mastodon.social/@podcastlinux/110598252144318467) para editar sonido
+ Compartir la [conexión de tu móvil por usb](https://mastodon.social/@podcastlinux/110603915161004830) para instalaciones de Arch Linux
+ Atareao se pasa a [Arch Linux](https://mastodon.social/@podcastlinux/110614295641904059)
+ Sintetizador libre [ZynAddSubFX](https://mastodon.social/@podcastlinux/110625619987246048) que funciona perfectamente con teclados MIDI en GNU/Linux
+ [FET](https://mastodon.social/@podcastlinux/110637888316858828), un generador de horarios académico libre con un potencial y configuración muy profesional
+ ¿Cuál es tu [navegador libre](https://mastodon.social/@podcastlinux/110643318371751765) y favorito?

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
