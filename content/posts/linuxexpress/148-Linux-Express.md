---
title: "#148 Linux Express"
date: 2022-06-22
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/148linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4389851
  mlength : 4276391
  iduration : "00:08:54"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE148
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/148linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/148linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #158 [Linux Connexion con esLibre2022](https://podcastlinux.com/PL158)
+ Siguiente episodio: Especial 6º Temporada
+ Publicaciones automatizadas con [Nitter](https://nitter.net/podcastlinux) + [rss2tg](https://rss2tg.duck.consulting)
+ [Enchufe Athom](https://twitter.com/podcastlinux/status/1536571082911764480) con Tasmota preflasheado
+ Placa [Micro:bit](https://twitter.com/podcastlinux/status/1534037783068741634) en el cole
+ Colaboración en podcasts [Mancomun Hardware Libre](https://www.mancomun.gal/es/noticias/mancomun-podcast-84-segundo-especial-sobre-hardware-libre) y Charlas [24H24L](https://fediverse.tv/w/rV8GgBXZJwB42TaJky9pN7)
+ Escuchando [Últimos de Feedlipinas](https://ultimosfeed.blogspot.com/)
+ Comparte archivos grandes con el servicio [Lufi](https://upload.disroot.org/)
+ [Akademy 2022](https://akademy.kde.org/2022) en Barcelona

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
