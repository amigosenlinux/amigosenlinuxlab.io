---
title: "#133 Linux Express"
date: 2021-11-24
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/133linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4869271
  mlength : 4805527
  iduration : "00:10:00"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE133
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/133linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/133linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #143 [Retrogaming y GNU/Linux](https://podcastlinux.com/PL143)
+ Siguiente episodio Linux Connexion con [Ainus](https://misjuegosenlinux.blogspot.com/)
+ Curso sobre [Kdenlive](https://podcastlinux.com/kdenlive)
+ Ordenador en el cole para [producción multimedia](https://twitter.com/podcastlinux/status/1460490275017658368)
+ Evento [Akademy-es](https://www.kde-espana.org/akademy-es-2021-en-linea/programa)
+ Charla con [Yoyo Fernández](https://salmorejogeek.com/2021/11/07/charlas-salmorejo-geek-juan-febles-de-podcast-linux-repasando-trayectoria-gnu-linux-y-software-libre-por-montera/)
+ Black Friday en [Slimbook](https://slimbook.es/) y [Vant](https://www.vantpc.es/)
+ Episodio sobre Software Libre de [La Hora Maker](https://podcasts.apple.com/es/podcast/software-libre-y-movimiento-maker/id1056919335?i=1000541169315) de Elsatch


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
