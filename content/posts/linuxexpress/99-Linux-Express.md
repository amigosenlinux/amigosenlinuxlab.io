---
title: "#99 Linux Express"
date: 2020-08-05
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/99linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5724984
  mlength : 5508850
  iduration : "00:11:12"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE99
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/99linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/99linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #109 [Linux Connexion con Karla Pérez](https://podcastlinux.com/PL109)
+ Siguiente episodio: Linux Connexion con [Carlos Arturo Guerra Parra](https://twitter.com/GuerraCarlosA)
+ Últimas actualizaciones en [podcastlinux.com](https://podcastlinux.com)
+ Ya es mío el [Vant Edge](https://www.vantpc.es/edge)
+ Nuevo [KDE Slimbook III](https://slimbook.es/kde-slimbook-amd)
+ [FLISOL A Coruña](https://flisol2020acoruna.gitlab.io/) y [esLibre](https://eslib.re/2020/) en septiembre
+ Actualizaciones de [Telegram](https://telegram.org/blog/profile-videos-people-nearby-and-more/es?ln=a)
+ Cacharreando con mi consola [RS-97](https://twitter.com/podcastlinux/status/1286999814484496387) y el firmware libre [RetroFW](https://github.com/retrofw/retrofw.github.io)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
