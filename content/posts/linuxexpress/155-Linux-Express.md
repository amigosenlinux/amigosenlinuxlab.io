---
title: "#155 Linux Express"
date: 2022-09-28
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/155linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 7930316
  mlength : 7484646
  iduration : "00:15:35"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE155
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/155linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/155linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #165 [Anbernic RG350 M/P](https://podcastlinux.com/PL165)
+ Siguiente episodio: Linux Connexion con Eduardo Moreno
+ Llegan más componentes del [PC Reciclado de la Audiencia](https://podcastlinux.com/pcreciclado)
+ Horarios del colegio generados con [FET](https://lalescu.ro/liviu/fet/)
+ [TIC80](https://nitter.net/podcastlinux/status/1571016223743148034#m), una consola virtual de videojuegos
+ Prueba [Audacity 3.2](https://nitter.net/podcastlinux/status/1571386035438747649#m)
+ [Micro:bit y Arduino]() en el colegio
+ Premios [Jpod](https://nitter.net/podcastlinux/status/1567007183178321920#m) a mediados de octubre 
+ [Akademy-es](https://www.kdeblog.com/presentado-akademy-es-2022-en-barcelona-y-en-linea-akademyes.html) y [Akademy Internacional](https://akademy.kde.org/2022) en Barcelona en octubre 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
