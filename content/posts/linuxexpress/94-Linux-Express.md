---
title: "#94 Linux Express"
date: 2020-05-27
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/94linuxexpress
  olength : 5179034
  mlength : 6633472
  iduration : "00:13:33"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE94
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/94linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/94linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #104 [Linux Connexion con VoroMV](https://avpodcast.net/podcastlinux/voromv)
+ Siguiente episodio: Linux Connexion con [Gabriel Viso](https://anchor.fm/gvisoc)
+ Emitir vídeo con [Simple Screen Recorder](https://www.maartenbaert.be/simplescreenrecorder/live-streaming) y [Autistici](https://live.autistici.org/)
+ Libera tus obras en [archive.org](https://archive.org/)
+ Promoviendo [#EscritorioGNULinux](https://twitter.com/search?q=%23escritorioGNUlinux) en redes
+ ¿Me paso a [Kubuntu](https://kubuntu.org/)?
+ Sigo con [LMMS](https://hackmd.io/@podcastlinux/HkAOUZnYI)
+ Ya está aquí [Audacity 2.4](https://www.audacityteam.org/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
