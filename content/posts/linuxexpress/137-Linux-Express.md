---
title: "#137 Linux Express"
date: 2022-01-19
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/137linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5741139
  mlength : 5233727
  iduration : "00:10:53"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE137
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/137linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/137linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #147 [Proyectos Podcast Linux](https://podcastlinux.com/PL147)
+ Siguiente episodio: Linux Connexion con Pablo López
+ [Non-Mixer](https://liberaturadio.org/non-mixer-mexcladora-viertual-para-jsck/): Una Mezcladora virtual para Jack
+ [Proyecto Libre Home Gym](https://gitlab.com/podcastlinux/librehomegym) para realizar ejercicios en casa
+ Crear [lanzador para las Appimage](https://ubunlog.com/crea-un-lanzador-de-aplicacion-archivos-appimage/)
+ Futuro de GNU/Linux: ¿QT, KDE, Wayland y Pipewire?
+ [Capturadora 4K de Slimbook](https://www.muylinux.com/2022/01/12/capturadora-4k-slimbook-linux/)
+ 20 de enero: [Día del Dominio Público](https://creativecommons.org/2021/12/09/public-domain-2022-join-us-20-january-for-a-celebration-of-sound/)
+ [Westen House](https://github.com/santiontanon/westen) el último juego libre de [Santiago Ontañón](https://sites.google.com/site/santiagoontanonvillar)



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
