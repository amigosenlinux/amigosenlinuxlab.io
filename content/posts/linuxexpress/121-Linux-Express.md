---
title: "#121 Linux Express"
date: 2021-06-09
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/121linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 7158702
  mlength : 6773488
  iduration : "00:14:06"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE121
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/121linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/121linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #131 [Especial Directo con Software Libre](https://podcastlinux.com/PL131)
+ Siguiente episodio: [Especial Es_Libre](https://eslib.re/2021/)
+ [Dock montado en el colegio](https://twitter.com/podcastlinux/status/1400352677444435968)
+ Buscando alternativas libres a ClubHouse.
+ [Concurso Custom PC Cacharreo Geek](https://www.spreaker.com/user/andresramos/la-hora-de-los-pc-custom)
+ [Firefox 89](https://news.itsfoss.com/firefox-89-release/) ya está aquí
+ [Slimbook One AMD](https://slimbook.es/one)
+ Nuevas versiones de [Shotcut](https://www.shotcut.org/blog/new-release-210518/) e [Inkscape](https://inkscape.org/es/news/2021/05/24/welcome-inkscape-11/)
+ Podcast [Programar Fácil](https://programarfacil.com/categoria/podcast)
+ Eventos a tener en cuenta: [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
