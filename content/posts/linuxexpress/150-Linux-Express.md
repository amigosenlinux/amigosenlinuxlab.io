---
title: "#150 Linux Express"
date: 2022-07-20
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/150linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5490932
  mlength : 5332992
  iduration : "00:11:06"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE150
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/150linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/150linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #160 [Linux Connexion con Jorge Lama](https://podcastlinux.com/PL160)
+ Siguiente episodio: Linux Connexion con Baltasar Ortega
+ Comparte tu [CamisetaGNULinux](https://twitter.com/podcastlinux/status/1545666967931179008)
+ Nueva [intro](https://nitter.net/podcastlinux/status/1547463154891898880) para la próxima temporada
+ Yoyo Fernández prueba [KDE Plasma de nuevo](https://salmorejogeek.com/2022/07/02/gnomero-prueba-kde-plasma-despues-de-mucho-tiempo-fuera-y-os-cuenta-que-tal-manjaro-plasma-21-3-1)
+ Añadir privilegios [root en Dolphin](https://nitter.net/podcastlinux/status/1544916262773456897)
+ Actualización de [Kdenlive 22.04.3](https://kdenlive.org/en/2022/07/kdenlive-22-04-3-released)
+ Nuevo [Slimbook Executive 2022](https://slimbook.es/executive)
+ Nuevos episodios de [KDE España](https://www.ivoox.com/podcast-podcast-kde-espana_sq_f1249423_1.html) y [KDE Express](https://kdeexpress.gitlab.io)
+ [Akademy-es](https://www.kdeblog.com/presentado-akademy-es-2022-en-barcelona-y-en-linea-akademyes.html) y [Akademy](https://akademy.kde.org/2022) en Barcelona en octubre 
Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
