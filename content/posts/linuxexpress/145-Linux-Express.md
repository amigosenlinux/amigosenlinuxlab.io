---
title: "#145 Linux Express"
date: 2022-05-11
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/145linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5201081
  mlength : 4889746
  iduration : "00:10:11"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE145
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/145linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/145linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #155 [ESP-32](https://podcastlinux.com/PL155)
+ Siguiente episodio: Linux Connexion con [Academía de Automatización del Hogar](https://campus.programarfacil.com/master-practico-domotica-automatizacion-e-iot/)
+ Empezando a trastear con [Tasmota](https://twitter.com/podcastlinux/status/1520277024543281152)
+ [Miniamplificador](https://twitter.com/podcastlinux/status/1518821652724948992) de audio para proyectos
+ Reciclando [Baterías 18650 para PowerBank](https://twitter.com/podcastlinux/status/1521351968932999168)
+ Conectar Twitter con Mastodon con [Moa](https://moa.party/)
+ Nueva actualización de [Kdenlive](https://twitter.com/podcastlinux/status/1522982811459899392)
+ [Slimbook Zero i-3](https://slimbook.es/zero)
+ [Vant  MOOVE3-15](https://www.vantpc.es/producto/moove3-15)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
