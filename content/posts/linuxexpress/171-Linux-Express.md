---
title: "#171 Linux Express"
date: 2023-05-10
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/171linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5014691
  mlength : 4738021
  iduration : "00:09:43"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE171
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/171linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/171linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #181 [Linux Connexion con Roberto Ruisánchez](https://podcastlinux.com/PL181)
+ Siguiente episodio: 6 meses con [Arch Linux](https://archlinux.org)
+ Probando [Vorta](https://mastodon.social/@podcastlinux/110268894953326754) para realizar copias de seguridad
+ Bajar la resolución de unas fotos desde la [terminal](https://mastodon.social/@podcastlinux/110281162846367384)
+ [cal -y](https://mastodon.social/@podcastlinux/110286826275055595) para echar un vistazo al calendario desde la terminal
+ [Tarjeta wifi + bluetooth](https://mastodon.social/@podcastlinux/110297206057294691) para mi Xeon chino
+ Probando [Synthing](https://mastodon.social/@podcastlinux/110308530607184418) para sincronizar carpetas
+ [Openverse](https://openverse.org/es), un directorio de obras Creative Commons
+ [Guía Arch Linux](https://arch.d3sox.me/) con consejos y trucos
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 12 y 13  de mayo
+ Evento [Akademy-es](https://mastodon.social/@podcastlinux/109982596234913476): Málaga el 9 y 10 de junio

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
