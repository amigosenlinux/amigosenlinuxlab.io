---
title: "#110 Linux Express"
date: 2021-01-06
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/110linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5926667
  mlength : 5792329
  iduration : "00:12:03"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE110
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/110linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/110linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #120 [Hardware Vant Block Lite](https://podcastlinux.com/PL120)
+ Siguiente episodio: Linux Connexion con [Lina Castro](https://twitter.com/lirrums)
+ Próximos episodios en 2021
+ Crear [USB booteable](https://twitter.com/Rikylinux/status/1343727493677801473) desde la terminal
+ [VOSK](https://alphacephei.com/vosk): Trascripción de voz a texto libre. 
+ Domótica libre con [Home Assistant](https://www.home-assistant.io/)
+ [Odroid Go Super](https://www.omgubuntu.co.uk/2020/12/odroid-go-super-ubuntu-handheld): Retro consola portátil con Ubuntu
+ Más información sobre un posible Flisol 2021 España online. ¿Te apuntas?

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
