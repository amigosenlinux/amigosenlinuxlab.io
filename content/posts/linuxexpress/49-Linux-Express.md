---
title: "#49 Linux Express"
date: 2018-09-03
author: Juan Febles
categories: [linuxexpress]
img: 2018/49linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/49linuxexpress
  olength : 3539418
  mlength : 4399104
  iduration : "00:08:59"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE49
---
Septiembre, vuelta a la rutina e inicio de la 3ª temporada.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/49linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/49linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Éxito [Maraton Linuxero 1º Aniversario](https://maratonlinuxero.org)
+ [Episodio #59 [Linux Connexion con Manz](https://avpodcast.net/podcastlinux/manz)
+ Próximo episodio: Software Libre
+ Nuevo logo, intro, cortinillas y fuentes en [Podcast Linux](https://avpodcast.net/podcastlinux)
+ Crea horarios escolares con [FET](https://www.lalescu.ro/liviu/fet/)
+ Nuevas [voces americanas y europeas](https://twitter.com/podcastlinux/status/1025021929739169794) para la 3ª Temporada 
+ Territorio f-Droid: [Flym](https://www.f-droid.org/en/packages/net.frju.flym/)
+ A un mes de las [Jpod18](http://jpod.es/)

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-295252/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
