---
title: "#149 Linux Express"
date: 2022-07-06
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/149linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5755833
  mlength : 5574363
  iduration : "00:11:36"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE149
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/149linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/149linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #159 [Especial 6º Temporada](https://podcastlinux.com/PL159)
+ Siguiente episodio: Linux Connexion con Jorge Lama
+ Crónica esLibre en [Mancomun Podcast](https://www.mancomun.gal/es/noticias/mancomun-podcast-85-reportaje-de-la-eslibre-edicion-2022/)
+ Configurando [Enchufes Athom](https://twitter.com/podcastlinux/status/1539109530935738368) con Tasmota preflasheado
+ Router Secundario para casa con [OpenWRT](https://twitter.com/podcastlinux/status/1542370985197854720)
+ No te pierdas [Últimos de Feedlipinas](https://ultimosfeed.blogspot.com/)
+ Nuevo [Slimbook Pro X Edición 2022](https://slimbook.es/prox)
+ Posible Curso Audacity en camino

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
