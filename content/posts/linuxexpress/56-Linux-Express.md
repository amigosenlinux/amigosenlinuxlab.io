---
title: "#56 Linux Express"
date: 2018-12-12
author: Juan Febles
categories: [linuxexpress]
img: 2018/56linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/56linuxexpress
  olength : 3417387
  mlength : 4100096
  iduration : "00:08:21"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE56
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/56linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/56linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#66 Especial Pinebook](https://avpodcast.net/podcastlinux/pinebook)
+ Próximo episodio: Especial Viaje a Valencia
+ Grabación en movilidad: [Audio Recorder + Cable OTG + Micrófono USB](https://twitter.com/podcastlinux/status/1069513804734742528)
+ Colaboración con Asociación Podcast
+ Territorio f-Droid: [Open camera](https://f-droid.org/en/packages/net.sourceforge.opencamera/)
+ ¿Qué movil Xiaomi me compro?
+ Una tablet GNU/Linux
+ [Maratón Linuxero](https://maratonlinuxero.org/) el 15 de diciembre

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-2533813/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
