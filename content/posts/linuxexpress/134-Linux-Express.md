---
title: "#134 Linux Express"
date: 2021-12-08
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/134linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6187284
  mlength : 5960767
  iduration : "00:12:24"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE134
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/134linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/134linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #144 [Linux Connexion con Ainus](https://podcastlinux.com/PL144)
+ Siguiente [Hardware Powkiddy Q90](https://powkiddy.com/products/powkiddy-q90-3-inch-ips-screen-handheld-console-dual-open-system-game-console-16-simulators-retro-ps1-kids-gift-3d-new-games)
+ Buscando consolas de 2º mano para instalarle RetroArch
+ Nuevo dispositivo para otro episodio Hardware
+ Charlas [Akademy-es](https://www.youtube.com/c/kdeespana/videos)
+ Curso [Kdenlive](https://podcastlinux.com/kdenlive)
+ [Proyecto sonoro](https://twitter.com/podcastlinux/status/1464519831387000836) para ejercicios en casa
+ Probando [sintetizadores de voz libres](https://twitter.com/podcastlinux/status/1466293054478950400)
+ Pixabay con [música](https://pixabay.com/es/music/) y [efectos de sonido](https://pixabay.com/es/sound-effects/) libres 


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
