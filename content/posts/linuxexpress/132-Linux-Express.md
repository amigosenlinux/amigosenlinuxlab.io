---
title: "#132 Linux Express"
date: 2021-11-10
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/132linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4754227
  mlength : 4691425
  iduration : "00:09:46"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE132
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/132linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/132linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #142 [I Encuentro Podcast Linux](https://podcastlinux.com/PL142)
+ Siguiente episodio Retrogaming y Software Libre
+ Curso sobre [Kdenlive](https://podcastlinux.com/kdenlive)
+ Colaboración [24H24L](https://24h24l.org/)
+ Nueva [Raspberry Pi Zero 2 W](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/)
+ [KDE Plasma 5.24](https://9to5linux.com/kde-plasma-5-24-desktop-environment-to-introduce-support-for-fingerprint-readers) traerá soporte de huella dactilar
+ [Audacity 3.1](https://www.audacityteam.org/audacity-3-1-is-out-now/)
+ Nuevo portátil [Vant Agile](https://www.vantpc.es/producto/agile)
+ Evento [Akademy-es](https://www.kde-espana.org/category/eventos/akademy-es-2021)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
