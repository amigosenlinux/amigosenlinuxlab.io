---
title: "#24 Linux Express"
date: 2017-09-20
categories: [linuxexpress]
author: Juan Febles
img: 2017/24LinuxExpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/24linuxexpress
  olength : 6004632
  mlength : 6052500
  iduration : "00:12:36"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE24
---
Hoy empieza el otoño y nos metemos en faena.Muchas cosas que contar.  

<audio controls>
  <source src="https://archive.org/download/linuxexpress/24linuxexpresss.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #34 Directo Maratón Linuxero](http://avpodcast.net/podcastlinux/maraton).
+ Próximo episodio Formatos Libres.
+ Finalistas [Premios Asociación Podcast](http://premios.asociacionpodcast.es/2017_finalistas/).
+ Nos mudamos a Gitlab:  <https://podcastlinux.gitlab.io>
+ Migración a GNU/Linux de un pc de mi colegio.
+ Idea: Un curso sobre crear tu propio podcast.

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
