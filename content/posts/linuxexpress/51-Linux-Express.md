---
title: "#51 Linux Express"
date: 2018-10-03
author: Juan Febles
categories: [linuxexpress]
img: 2018/51linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/51linuxexpress
  olength : 4370454
  mlength : 5373952
  iduration : "00:10:58"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE51
---
Atención, recuerda que tenemos nuevas empresas colaboradoras, [Chicles GUW](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga).

<audio controls>
  <source src="https://archive.org/download/linuxexpress/51linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/51linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ [Episodio [#61 Linux Connexion con RikyLinux](https://avpodcast.net/podcastlinux/rikylinux)
+ Próximo episodio: Especial [Jpod18](http://jpod.es/)
+ Gracias a las Empresas colaboradoras: [Chicles WUG](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga)
+ Actualización [KDE Neon](https://neon.kde.org/)
+ [System76](https://system76.com/) y su [Thelio](https://thel.io/) Open Source.
+ Diseños GNU/Linux en [Linux.pictures](https://linux.pictures/)
+ Territorio f-Droid: [Siete](https://f-droid.org/es/packages/de.baumann.sieben/)
+ Futuros programas.

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [https://pixabay.com/photo-2322811/). Los logos WUG, Waterdrop Hydroprint y Jpod18Madrid son propiedad de sus respectivas empresas.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
