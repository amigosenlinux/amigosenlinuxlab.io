---
title: "#53 Linux Express"
date: 2018-10-31
author: Juan Febles
categories: [linuxexpress]
img: 2018/53linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/53linuxexpress
  olength : 4519024
  mlength : 5468160
  iduration : "00:11:11"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE53
---
Agradecer una vez más a las empresas colaboradoras del programa: [Chicles GUW](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga).

<audio controls>
  <source src="https://archive.org/download/linuxexpress/53linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/53linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#63 Linux Connexion con Rubén Rodríguez](https://avpodcast.net/podcastlinux/rubenrodriguez)
+ Próximo episodio: Pásate a GNU/Linux
+ Gracias de nuevo a las Empresas colaboradoras: [Chicles WUG](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga)
+ ya viene mi [Pinebook](https://www.pine64.org/?product=11-6-pinebook)
+ [Pinephone y Pinetab](https://itsfoss.com/pinebook-kde-smartphone/) con KDE Plasma para 2019
+ Territorio f-Droid: [StreetComplete](https://f-droid.org/es/packages/de.westnordost.streetcomplete/)
+ En marcha FLISoL Tenerife ¿Y el tuyo?
+ Visita a Valencia en diciembre.
+ [Maratón Linuxero](https://maratonlinuxero.org/) el 15 de diciembre

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [https://pixabay.com/photo-2071296). Los logos WUG y Waterdrop Hydroprint son propiedad de sus respectivas empresas.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
