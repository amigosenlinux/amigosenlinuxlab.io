---
title: "#101 Linux Express"
date: 2020-09-02
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/101linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5407040
  mlength : 5278030
  iduration : "00:10:59"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE101
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/101linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/101linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #111 [Linux Connexion con José Jiménez](https://podcastlinux.com/PL111)
+ Siguiente episodio: Hardware: Nuevo set de Escritorio
+ Doble monitor y soporte para éstos.
+ Evento [24H24L](https://www.24h24l.org/)
+ Súmate al [#ViernesDeEscritorio #EscritorioGNULinux](https://twitter.com/hashtag/ViernesDeEscritorio)
+ [Akademy](https://akademy.kde.org/2020), [FLISOL A Coruña](https://flisol2020acoruna.gitlab.io/) y [esLibre](https://eslib.re/2020/) en septiembre
+ [OSHWDem 2020](https://oshwdem.org/) Edición online
+ Cacharreando con [RetroArch](https://twitter.com/podcastlinux/status/1298145022957715457)
+ [SuperTux Kart 1.2](https://supertuxkart.net/Main_Page)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
