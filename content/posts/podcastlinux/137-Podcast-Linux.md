---
title: "#137 Linux Connexion con Luis Fajardo"
date: 2021-08-25
author: Juan Febles
categories: [podcastlinux]
img: PL/PL137.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL137
  image: https://podcastlinux.gitlab.io/images/PL/PL137.png
  olength : 33387507
  mlength : 38509197
  iduration : "01:03:26"
tags: [Linux Connexion, Luis Fajardo, podcastlinux]
comments: true
aliases:
  - /PL137
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL137.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL137.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega de Podcast Linux, la número 137. Un saludo muy fuerte de quien te habla, Juan Febles. Terminamos las charlas de verano con Luis Fajardo, Profesor de Derecho civil en la Universidad de La Laguna, comenzó como tal en 1995 en la Autónoma de Madrid (UAM). Ha sido Abogado, Juez, y DPD, delegado de Protección de Datos. Usa nuevas tecnologías tempranamente en la UAM, Universidad de Almería y de Gerona, significándose por la defensa de las libertades en el cibersepacio, participando en diversas asociaciones como el Centro de Alternativas Legales, MadridWireless, GULIC , ESLIC; y en proyectos como el portal informativo Eurolaw-Consulting y su plataforma on-line para bufetes. Actualmente administra diversos nodos del Fediverso (Diaspora, Mastodon, Peertube,...) y el sistema de videoconferencias Big Blue Button de EDUCATIC, asociación que impulsa el proyecto Tecnología para la Sociedad (TXS.es)  

Recordar a los oyentes que no estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla sino en [Big Blue Button](https://bbb.educar.encanarias.info/b), otro servicio libre para videoconferencias, y que este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.



**Enlaces:**  
+ Web: <https://www.educar.encanarias.info/proyecto/>  
+ BBB: <https://bbb.educar.encanarias.info/b>  
+ Mastodon: <https://txs.es/@lfajardo>  

**La imagen utilizada en la portada**, la fotografía de Luis Fajardo, es propiedad del él mismo y ha sido cedida para la carátula del episodio.
  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Alez Productions - Innovative Corporate: <https://www.free-stock-music.com/alex-productions-innovative-corporate-economy.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
