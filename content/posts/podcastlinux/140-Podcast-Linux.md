---
title: "#140 Linux Connexion con Luis del Valle"
date: 2021-10-06
author: Juan Febles
categories: [podcastlinux]
img: PL/PL140.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL140
  image: https://podcastlinux.gitlab.io/images/PL/PL140.png
  olength : 36399931
  mlength : 37958165
  iduration : "01:02:29"
tags: [Linux Connexion, Luis del Valle, podcastlinux]
comments: true
aliases:
  - /PL140
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL140.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL140.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega, la número 140, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy vuelve con nosotros Luis del Valle, apasionado del Movimiento Maker, está detrás del blog Programarfacil.com, del podcast La tecnología para Todos y del Campus de Programarfacil. Hoy hablaremos de Home Assistant y cómo iniciarte en él.  

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y que este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.


**Enlaces:**  
+ Web: <https://programarfacil.com/>  
+ Curso Domótica: <https://campus.programarfacil.com/master-practico-domotica-automatizacion-e-iot/>  
+ Podcast La Tecnología para Todos (Feed): <https://programarfacil.com/podcast>
+ Canal Telegram: <https://t.me/programarfacilc>  
+ Home Assistant: <https://www.home-assistant.io/getting-started/>  
+ Room Asistant: <https://www.room-assistant.io/>  

**La imagen utilizada en la portada**, la fotografía de Luis del Valle, es propiedad del él mismo y ha sido cedida para la carátula del episodio.
  
Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Scandinavianz - Paraglide: <https://www.free-stock-music.com/scandinavianz-paraglide.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
