---
title: "#153 Linux Connexion con Atareao"
date: 2022-04-06
author: Juan Febles
categories: [podcastlinux]
img: PL/PL153.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL153
  image: https://podcastlinux.gitlab.io/images/PL/PL153.png
  olength : 33573921
  mlength : 36432754
  iduration : "00:59:38"
tags: [PodcastLinux, Proyectos]
comments: true
aliases:
  - /PL153
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL153.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL153.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega, la número 153, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy vuelve con nosotros Lorenzo Carbonell, Bloguero, podcaster y desarrollador en GNU/Linux, lo puedes escuchar cada lunes y jueves en su podcast y leer cuando quieras en [atareao.es](https://www.atareao.es/)  

**Enlaces:**  
+ Blog Atareao: <https://www.atareao.es/>  
+ Podcast Atareao: <https://www.atareao.es/podcasts/>  
+ Mastodon: <https://atareao.es/mastodon>  
+ Twitter: <https://twitter.com/atareao>  
+ Fediverse.tv: <https://fediverse.tv/c/atareao>  
+ Youtube: <https://www.youtube.com/c/atareao>  
+ Github: <https://github.com/atareao>  
+ Canal de Telegram: <https://t.me/canal_atareao>  


**Las imagen utilizada en la portada** es propiedad de Lorenzo Carbonel y ha sido cedida expresamente para este episodio.  

  
Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Penguinmusic - Inspiring Digital Technology: <https://pixabay.com/es/music/late-inspiring-digital-technology-14191>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
