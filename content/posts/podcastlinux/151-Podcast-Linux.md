---
title: "#151 Linux Connexion con David Vargas"
date: 2022-03-09
author: Juan Febles
categories: [podcastlinux]
img: PL/PL151.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL151
  image: https://podcastlinux.gitlab.io/images/PL/PL151.png
  olength : 34759719
  mlength : 39472312
tags: [Linux Connexion, Asker, David vargas, podcastlinux]
comments: true
aliases:
  - /PL151
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL151.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL151.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega de Podcast Linux, la número 151. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy está con nosotros David Vargas, Ingeniero Informático trabaja de profesor de FP Especialidad: Sistemas y Aplicaciones informáticas desde 2004. Es un apasionado de GNU/Linux y Ruby lover. Hoy nos viene a hablar del proyecto Asker, una herramienta de software libre que se encarga de reducir las tareas repetitivas y monótonas para buscar la felicidad del profesorado.

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y que este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.


Enlaces:

Asker software:
* Rubygem: <https://rubygems.org/gems/asker-tool>
* GitHub: <https://github.com/teuton-software/asker>
* Telegram: <https://t.me/+FsuphhJSaaGVpKcv>

Asker inputs:
* GitHub: <https://github.com/dvarrui/asker-inputs>

Contacto:
* Twitter:<https://twitter.com/dvarrui>
* Email: <dvarrui@protonmail.com>



**La imagen utilizada en la portada**, la foto de David Vargas, es propiedad de él mismo y ha sido cedida expresamente para la carátula de este episodio.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Coma-Media - Commercial Ambient: <https://pixabay.com/es/music/corporativo-commercial-ambient-18392>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
