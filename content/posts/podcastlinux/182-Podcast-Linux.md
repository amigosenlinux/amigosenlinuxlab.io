---
title: "#182 6 meses con Arch Linux"
date: 2023-05-17
author: Juan Febles
categories: [podcastlinux]
img: PL/PL182.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL182
  image: https://podcastlinux.gitlab.io/images/PL/PL182.png
  olength : 25906644
  mlength : 24701113
  iduration : "00:40:56"
tags: [podcastlinux]
comments: true
aliases:
  - /PL182
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL182.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL182.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!
Bienvenido a otra entrega, la número 182, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles.

Hoy voy a compartir mi experiencia con [Arch Linux](https://archlinux.org), una distro de la que se está hablando mucho últimamente y de la que quiero explicar qué pasos he dado para disfrutar de ella.

Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 182 Especial Podcasting 2.0.

**Enlaces:**

+ Arch Linux: <https://archlinux.org>
+ ArchWiki: <https://wiki.archlinux.org/>
+ Tutorial Computadoras y sensores: <https://youtu.be/L2y59_jq3Zo>
+ InstalaArch.txt: <https://gitlab.com/podcastlinux/scripts/-/blob/master/InstalaArch.txt>
+ PostInstalaArch.sh: <https://gitlab.com/podcastlinux/scripts/-/blob/master/PostInstalaArch.sh>
+ Salmorejo Geek:<https://salmorejogeek.com/>


**La imagen utilizada en portada**, el logotipo de Arch Linux, es propiedad de su Comunidad.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ FSM Team - Positive Day: <https://www.free-stock-music.com/fsm-team-positive-day.html>

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
