---
title: "#129 Hardware Thinkpad"
date: 2021-05-05
author: Juan Febles
categories: [podcastlinux]
img: PL/PL129.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL129
  image: https://podcastlinux.gitlab.io/images/PL/PL129.png
  olength : 24189860
  mlength : 25611043
  iduration : "00:42:19"
tags: [Hardware, Thinkpad, podcastlinux]
comments: true
aliases:
  - /PL129
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL129.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL129.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!  
Mi nombre es Juan Febles y hoy tenemos un episodio sobre una gama de portátiles todoterreno. En menos de media hora te daré a conocer la serie Thinkpad de Lenovo, un portátil de 2ª mano con unas muy buenas prestaciones que le sienta GNU/Linux de maravilla.
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 129, Hardware ThinkPad.

Recordar a los oyentes que este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.



**Enlaces:**  
+ Grupo Telegra Thinkpad_es: <https://t.me/Thinkpad_es>
+ Episodio X220: <https://podcastlinux.com/PL09>
+ <https://www.crucial.es/compatible-upgrade-for/lenovo/thinkpad-t440>
+ <https://en.wikipedia.org/wiki/ThinkPad>
+ <https://es.wikipedia.org/wiki/ThinkPad>
+ <https://www.lenovo.com/es/es/thinkpad>
+ Videotutoriales T440P: <https://youtube.com/playlist?list=PL-2mqY82TErgoto9FvdxQHogH3J96ZAEY>
+ Videotutoriales x220/x230: <https://youtube.com/playlist?list=PL-2mqY82TErj-pa3O8xRnxUZdlr5qcoJl>
+ Videotutoriales x240: <https://youtube.com/playlist?list=PL-2mqY82TErhDl7XRFDLW3zLUzGaEF48V>
+ <https://octoperf.com/blog/2018/11/07/thinkpad-t440p-buyers-guide/#buying-a-unit>
+ <https://2000c43.com/blog/usbct440p>
+ <https://www.cafecondebian.com/habilitar-lector-de-huellas-dactilares>

**La imagen utilizada en la portada**, se han liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/images/id-1019820/>  
El logotipo ThinkPad es marca registrada de la empresa Lenovo.  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
John Tasoulas - The Dead: <https://www.free-stock-music.com/john-tasoulas-the-dead.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
