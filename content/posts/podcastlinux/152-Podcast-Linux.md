---
title: "#152 Paqueterías multidistribución GNU/Linux"
date: 2022-03-23
author: Juan Febles
categories: [podcastlinux]
img: PL/PL152.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL152
  image: https://podcastlinux.gitlab.io/images/PL/PL152.png
  olength : 12414925
  mlength : 12892769
  iduration : "00:21:06"
tags: [PodcastLinux, Proyectos]
comments: true
aliases:
  - /PL152
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL152.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL152.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega de Podcast Linux, la número 152 . Un saludo muy fuerte de quien te habla, Juan Febles.
Hoy vamos a echrale un vistazo a las nuevas paqueterías GNU/Linux: AppImage, Snap y Flatpak.
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 152, Paqueterías Muktidistribución GNU/Linux.  

**Enlaces:**  
+ Appimage: <https://appimage.org>
+ AppImageHub: <https://www.appimagehub.com>
+ Snap: <https://snapcraft.io>
+ Flatpak: <https://www.flatpak.org>
+ Atareao: <https://atareao.es>

**Las imágenes utilizadas en la portada**, los logotipos de Appimage, Snap y Flatpak son propiedad de cada una de estos proyectos.  

  
Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Coma-Media - Corporate Harmonics: <https://pixabay.com/es/music/corporativo-corporate-harmonics-18394>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
