---
title: "#155 ESP32"
date: 2022-05-04
author: Juan Febles
categories: [podcastlinux]
img: PL/PL155.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL155
  image: https://podcastlinux.gitlab.io/images/PL/PL155.png
  olength : 15553714
  mlength : 16578107
  iduration : "00:26:23"
tags: [ESP32, Luis del Valle, Germán Martín, podcastlinux]
comments: true
aliases:
  - /PL155
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL155.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL155.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega de Podcast Linux, la número 155 . Un saludo muy fuerte de quien te habla, Juan Febles.
Hoy vamos a echrale un vistazo a la ESP32, El SOC para placas de desarrollo con Wifi y Bluetooth que ha revolucionado el mundo maker y la domótica con Software Libre.
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 155, ESP32.

**Enlaces:**  
<https://programarfacil.com/esp8266/esp32/>  
<https://programarfacil.com/esp8266/programar-esp32-ide-arduino/>  
<https://www.luisllamas.es/esp32/>  
<https://www.prometec.net/instalando-esp32/>  
<https://domoticaencasa.es/esp32/>  
<https://randomnerdtutorials.com/projects-esp32/>  
<https://tecnotizate.es/instalacion-y-primer-uso-de-esphome-en-home-assistant/>  

**Las imagen utilizada en la portada**, se han liberado desde [Wikimedia](https://commons.wikimedia.org) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://commons.wikimedia.org/wiki/File:ESP32_Espressif_ESP-WROOM-32_Dev_Board.jpg>.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
ComaStudio - Figure: <https://pixabay.com/es/music/corporativo-figure-109396>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
