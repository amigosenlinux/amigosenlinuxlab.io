---
title: "#143 Retrogaming y GNU/Linux"
date: 2021-11-17
author: Juan Febles
categories: [podcastlinux]
img: PL/PL143.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL143
  image: https://podcastlinux.gitlab.io/images/PL/PL143.png
  olength : 16193828
  mlength : 16849182
  iduration : "00:27:15"
tags: [Retrogaming, Retroarch, podcastlinux]
comments: true
aliases:
    - /PL143
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL143.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL143.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!
Bienvenido a otra entrega de Podcast Linux, la número 143. Un saludo muy fuerte de quien te habla, Juan Febles.
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 143 Retrogaming.  

**Enlaces de interés:**  
+ Libretro API: <https://www.libretro.com/>
+ Retroarch: <https://www.retroarch.com/>
+ Retropie: <https://retropie.org.uk/>
+ 4Mhz: <https://www.4mhz.es/>
+ Mojon Twins: <https://www.4mhz.es/>
+ Santiago Ontañón: <https://github.com/santiontanon>
+ MISTer FPGA: <https://www.antoniovillena.es/store/product-category/mister/>
+ Ainus: <https://misjuegosenlinux.blogspot.com/>


**La imagen utilizada en la portada**, se han liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/images/id-2856330/>.  


Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Joshua McLean - Mountain Trials: <https://www.free-stock-music.com/joshuamclean-mountain-trials.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
