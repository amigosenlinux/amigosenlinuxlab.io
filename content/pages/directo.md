---
title: Directo
---
<!-- BEGINS: AUTO-GENERATED MUSES RADIO PLAYER CODE -->
<script src="https://hosted.muses.org/mrp.js"></script>
<script>
MRP.insert({
'url':'http://podcastlinux.ddns.net:8000/live',
'lang':'es',
'codec':'ogg',
'volume':75,
'autoplay':true,
'forceHTML5':true,
'jsevents':true,
'buffering':0,
'title':'Podcast Linux',
'welcome':'Bienvenidos',
'wmode':'transparent',
'skin':'arvyskin',
'width':253,
'height':100
});
</script>
<!-- ENDS: AUTO-GENERATED MUSES RADIO PLAYER CODE -->
<p><iframe style="height: 600px; width: 1000px; border: 0px;" src="https://live.autistici.org/#podcastlinux" width="300" height="150"></iframe></p>
<iframe src="https://kiwiirc.com/client/chat.freenode.net/?&theme=mini#podcastlinux" style="border:0; width:100%; height:450px;"></iframe>



Puedes <strong>contactar</strong> de las siguientes maneras:
<ul>
 	<li>Twitter: <a href="https://twitter.com/podcastlinux">@podcastlinux</a></li>
 	<li>Mastodon: <a href="https://mastodon.social/@podcastlinux">@podcastlinux</a></li>
 	<li>Correo: <a href="mailto:podcastlinux@disroot.org">podcastlinux@disroot.org</a></li>
 	<li>Archive.org: <a href="https://archive.org/details/@podcast_linux">@podcast_linux</a></li>
 	<li>Gitlab: <a href="https://gitlab.com/podcastlinux">@podcastlinux</a></li>
 	<li>Web: <a href="https://podcastlinux.com">podcastlinux.com</a></li>
</ul>

Somos más de 1.000 los que estamos en el canal de <a href="https://telegram.me/podcastlinux">Telegram</a>  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en <a href="https://telegram.me/podcastlinux">Telegram.me/podcastlinux</a>
<br>

Pásate si quieres también por el canal de <a href="https://www.youtube.com/podcastlinux/videos">Youtube</a> para visualizar mis Screencasts.
<br>

Me encantan los comentarios que me dejan tanto en la <a href="https://podcastlinux.com">web</a> como en <a href="http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html">Ivoox</a>. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.
<br>

No te olvides suscribirte en <a href="http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html">Ivoox</a>, <a href="https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2">Itunes</a> para no perderte ninguno de mis episodios.
<br>

El feed del programa oficial: <https://podcastlinux.com/feed>

Y el feed de Linux Express: <https://podcastlinux.com/Linux-Express/feed>
