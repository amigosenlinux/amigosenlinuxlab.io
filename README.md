# PodcastLinux
Este repositorio es la web del proyecto Podcast Linux, un podcast del mundo GNU/Linux para amantes del Software Libre. 

## Servicios y Aplicaciones libres
Esta web es una realidad gracias a:  
Gitlab Pages: <https://docs.gitlab.com/ee/user/project/pages/>  
Hugo Go: <https://gohugo.io/>  
Git: <https://git-scm.com/>  
Markdown: <https://es.wikipedia.org/wiki/Markdown>  
Audacity: <https://www.audacityteam.org/>  
Ardour: <https://ardour.org/>  
FFmpeg: <https://ffmpeg.org/>  
Kid3-cli: <https://kid3.kde.org/>  
GIMP: <https://www.gimp.org/>  
Inkscape: <https://inkscape.org/>  
ShotCut: <https://www.shotcut.org/>  
LMMS: <https://lmms.io/>  
Pixabay: <https://pixabay.com/>  
Jamendo: <https://www.jamendo.com/>  
Free Stock Music: <https://www.free-stock-music.com/>  
Freesound: <https://freesound.org/>  


## ¿Quieres colaborar?
Si quieres realizar alguna sugerencia o mejora, soy todo oídos. Muchas personas antes lo han hecho y gracias a tanta gente esta web es una realidad.  
No dudes en ponerte en contacto conmigo.  

## Métodos de Contacto
Nada me hace más feliz que te pongas en contacto conmigo.  
Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

## Agradecimientos
Esta nueva web ha sido un trabajo titánico de [David Marzal](https://gitlab.com/Marzal).  
En anteriores versiones han colaborado [Hefistión](https://gitlab.com/hefistion), [UGeek](https://github.com/ugeek) y [Alejandro Domínguez](https://gitlab.com/aledomu).  

Gracias a todos por ser tan pacientes y allanar el camino.


## Licencia
Todo el contenido propio del podcast se libera bajo Licencia [Creative Commons Atribución - Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/)  
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
