#!/bin/bash
set -eEuo pipefail
# Este comando anterior activa el modo estricto en BASH, si algo falla para la ejecución del script

# Colores a usar con el echo -e
HL='\033[1;32m'   # Bold Green
WL='\033[1;33m'   # Bold Yellow
NL='\033[0m'      # Text Reset

GITLAB="$HOME/Documentos/podcastlinux.gitlab.io"

f_convertir () {

    echo "$PWD" # Mostramos en la carpeta en la que estamos (DEBUG)
    ffmpeg -hide_banner -y -i "$FUENTE" -metadata title="$TITLE" -metadata artist="Podcast Linux" -metadata genre="Podcast" -metadata track="$PISTA" -metadata copyright="CC BY - SA 4.0" -metadata year="$(date +"%Y")" -metadata comment="Podcast realizado por Juan Febles con $GRABADOR" -metadata language=esp -ar 44100 -ac 1 -ab "${BITRATE}k" "$DESTINO.$FORMATO"

    # -i $IMAGEN -map_metadata 0 -map 0 -map 1
    # ffmpeg no se lleva bien con meter caratulas en ogg, en mp3 funciona con el anterior comando
    kid3-cli -c "set picture:${IMAGEN} 'Portada'" "$DESTINO.$FORMATO"
}

f_metadatos() {
    RUTAART="$GITLAB/content/posts/$SECCION/$episodio-$ARTICULO"

    f_nuevohugo # Lanzamos la función de crear articulos

    if [[ $FORMATO == "ogg" ]]; then
        olength=$(wc -c "$DESTINO.ogg" | awk '{print $1}')
        sed -i "s/olength.*/olength : $olength/" "$RUTAART"
    else
        mlength=$(wc -c "$DESTINO.mp3" | awk '{print $1}')
        sed -i "s/mlength.*/mlength : $mlength/" "$RUTAART"
        sed -i "s/title:.*/title: \"$TITLE\"/" "$RUTAART"
    fi
    # Este dato es siempre el mismo estemos dentro del bucle for en MP3 u OGG
    iduration=$(ffprobe -hide_banner -i "$DESTINO.$FORMATO" 2>&1 | grep Duration| cut -c 13-20)
    sed -i "s/iduration.*/iduration : \"$iduration\"/" "$RUTAART"
}

f_nuevohugo() {
    # Comprobamos si ya existe el articulo
    if ! [[ -s $RUTAART ]]; then
        # Y si no existe lo creamos con el archetype correspondiente que sacamos $SECCION
        cd "$GITLAB"
        hugo new --kind "$SECCION" "posts/$SECCION/$episodio-$ARTICULO"
        cd "$DIRECTORIO"
    fi
}

f_sube_ia () {
    ia upload "$IA" "$DESTINO.ogg" "$DESTINO.mp3"
}

echo -e "${HL}¿${WL}(P)${HL}odcast Linux o ${WL}(L)${HL}inux Express?: ${NL}"
read -r podcast
echo -e "${HL}Episodio: ${NL}"
read -r episodio
echo -e "${HL}¿Quieres ${WL}(C)${HL}onvertir / ${WL}(M)${HL}etadatos / ${WL}(A)${HL}mbos / sólo ${WL}(S)${HL}ubir episodio?: ${NL}"
read -r r_acciones

if [[ $podcast == "P" ]] ;then
    echo -e "${HL}Título episodio: ${NL}"
    read -r titulo
    echo -e "${HL}¿Subo a Archive.org?: ${NL}"
    read -r r_ia

    # Ajustamos las variables y la carpeta en funcion de que tipo de podcast es para la ejecución de comandos
    cd "$HOME/Música/PL${episodio}/export/" || exit 1
    DIRECTORIO="$HOME/Música/PL${episodio}/export/"
    FUENTE="PL${episodio}.flac"
    IMAGEN="$HOME/Imágenes/PL${episodio}.png"
    TITLE="#$episodio $titulo"
    PISTA=$(("$episodio" + 1))
    DESTINO="PL${episodio}"
    SECCION='podcastlinux'
    ARTICULO='Podcast-Linux.md'
    BITRATE=80
    GRABADOR='Ardour'
    IA='podcast_linux'
    # Ejecutamos los comandos dos veces, una con FORMATO=mp3 y otra con FORMATO=ogg
    for FORMATO in mp3 ogg ; do
        # Solo ejecutamos los comandos en funcion de las respuestas que hemos dado
        [[ "$r_acciones" =~ ^(A|a|C|c)$ ]] && f_convertir
        [[ "$r_acciones" =~ ^(A|a|M|m)$ ]] && f_metadatos
        
    done

    [[ "$r_ia" =~ ^(SI|si|S|s|y|yes|Y|YES)$ ]] || [[ "$r_acciones" =~ ^(S|s)$ ]] && f_sube_ia
    
elif [[ $podcast == "L" ]] ;then
    echo -e "${HL}¿Subo a Archive.org?: ${NL}"
    read -r r_ia
    cd "$HOME/Música/" || exit 2
    DIRECTORIO="$HOME/Música/"
    FUENTE="${episodio}linuxexpress.flac"
    IMAGEN="$HOME/Imágenes/linuxexpress.png"
    TITLE="#$episodio Linux Express"
    PISTA="$episodio"
    DESTINO="${episodio}linuxexpress"
    SECCION='linuxexpress'
    ARTICULO='Linux-Express.md'
    BITRATE=64
    GRABADOR='Audacity'
    IA='linuxexpress'
    for FORMATO in mp3 ogg ; do
        [[ "$r_acciones" =~ ^(A|a|C|c)$ ]] && f_convertir
        [[ "$r_acciones" =~ ^(A|a|M|m)$ ]] && f_metadatos
    done
 
    [[ "$r_ia" =~ ^(SI|si|S|s|y|yes|Y|YES)$ ]] || [[ "$r_acciones" =~ ^(S|s)$ ]] && f_sube_ia
    
else
    echo -e "${WL}Debes elegir un Podcast${NL}"
fi

echo -e "${HL}FIN!${NL}"
